package com.demo_serenity.login;

import org.testng.annotations.Test;


import com.demo_serenity.base.SeleniumWrapper;
import com.demo_serenity.home.Home;
import com.demo_serenity.organization.BusinessUnits;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class TC001_Login {
	
	WebDriver driver;
	SeleniumWrapper seleniumWrapper;
	Login login;
	Home home;
	BusinessUnits business;

  @BeforeTest
  public void beforeTest() {
	  
	  seleniumWrapper = new SeleniumWrapper(driver);
		driver = seleniumWrapper.chromeDriverConnection();
		login = new Login(driver);
		home = new Home(driver);
		business = new BusinessUnits(driver);
  }
  
  @Test
  public void tC001_Login() {
	  
	  login.setup("https://demo.serenity.is/");
	  
	  login.setInputUsuario("admin");
	  
	  login.setInputPassword("serenity");
	  
	  login.clickButtonSignIn();
	  
	  home.validateTitleDashboard();
	  
	  home.clickOptionOrganization();
	  
	  home.clickOptionBusinessUnits();
	  
	  business.clickbuttonNewBusinessUnit();
	  
	  business.setBusinessUnit("Test Business Unit Automation","Marketing � Marketing Strategies","Marketing � Marketing Strategies");
	  
	  business.saveBusinessUnit();
  }

  @AfterTest
  public void afterTest() {
  }

}
