package com.demo_serenity.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.demo_serenity.base.SeleniumWrapper;

public class Login extends SeleniumWrapper{

	public Login(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	By inputUsuario = By.id("LoginPanel0_Username");	
	By inputPassword = By.id("LoginPanel0_Password");
	By buttonSignIn = By.id("LoginPanel0_LoginButton");
	
	public void setup(String url) {
		launchBrowser(url);
	}
	
	public void setInputUsuario(String nameUsuario)
	{
		clearFindElement(inputUsuario);
		type(nameUsuario, inputUsuario);
	}
	
	public void setInputPassword(String nameUsuario)
	{
		clearFindElement(inputPassword);
		type(nameUsuario, inputPassword);
	}
	
	public void clickButtonSignIn() 
	{
		click(buttonSignIn);
	}

}
