package com.demo_serenity.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.demo_serenity.base.SeleniumWrapper;

public class Home extends SeleniumWrapper{

	public Home(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	By labelDashboard = By.xpath("//*[@id='s-DashboardPage']//*[@class='content-header']/h1");
	By optionOrganization = By.xpath("//*[@id='nav_menu1_3']/li[1]/a/span");
	By optionBusinessUnits = By.xpath("//*[@id='nav_menu1_3_1']/li[1]/a/span");
	By labelBusinessUnits = By.xpath("//*[@id='GridDiv']/div[1]/div");
	By optionMeeting = By.xpath("//*[@id='nav_menu1_3']/li[2]/a/span");
	By optionMeetings = By.xpath("//*[@id='nav_menu1_3_2']/li[1]/a/span");

	
	public void validateTitleDashboard() {
		waitForElementPresent(labelDashboard);
	}
	
	public void clickOptionOrganization() {
		waitForElementPresent(optionOrganization);
		click(optionOrganization);
	}
	
	public void clickOptionBusinessUnits() {
		waitForElementPresent(optionBusinessUnits);
		click(optionBusinessUnits);
		waitForElementPresent(labelBusinessUnits);
	}
	
	public void clickOptionMeeting() {
		waitForElementPresent(optionMeeting);
		click(optionMeeting);
	}
	
	public void clickOptionMeetings() {
		waitForElementPresent(optionMeetings);
		click(optionMeetings);
	}
	
	
	
}
