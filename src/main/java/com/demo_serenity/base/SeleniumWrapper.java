package com.demo_serenity.base;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

public class SeleniumWrapper {
	
	protected WebDriver driver;

	public SeleniumWrapper(WebDriver driver) {
		this.driver = driver;

	}

	/**
	 * Chrome Driver Connection
	 **/
	public WebDriver chromeDriverConnection() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chrome/chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}

	/**
	 * Reporter Log
	 **/
	public void reportLog(String log) {
		Reporter.log(log);
	}

	/**
	 * implicitWait
	 **/
	public void implicitWait(int time) {
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	/**
	 * Launch Browser
	 **/
	public void launchBrowser(String url) {
		try {
			reportLog("Launch..." + url + "application");
			driver.get(url);
			driver.manage().window().maximize();
			implicitWait(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Find Element
	 **/
	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}

	/**
	 * Clear Element
	 **/
	public void clearFindElement(By locator) {
		driver.findElement(locator).clear();
	}

	/**
	 * Text Object
	 **/
	public void type(String inputTex, By locator) {
		driver.findElement(locator).sendKeys(inputTex);
	}

	/**
	 * Click Object
	 **/
	public void click(By locator) {
		driver.findElement(locator).click();
	}

	/**
	 * Wait For Element Present
	 **/
	public void waitForElementPresent(By locator) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

		} catch (TimeoutException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Hard Assertion
	 **/
	public void AssertEquals(String actualValue, String expectedValue) {
		try {
			Assert.assertEquals(actualValue, expectedValue);
		} catch (AssertionError e) {
			Assert.fail("Not able to assert actual value < " + actualValue + " > with expected value < " + expectedValue
					+ " >");
		}
	}

	/**
	 * SoftAssert
	 **/
	public void SoftAssert(String actualValue, String expectedValue) {
		try {
			SoftAssert assertion = new SoftAssert();
			assertion.assertEquals(actualValue, expectedValue);
			// assertion.assertAll();

		} catch (AssertionError e) {
			SoftAssert assertion = new SoftAssert();
			Assert.fail("SoftAssert - No encontrado");
			e.printStackTrace();
		}
	}

	/**
	 * Get Text
	 **/
	public String getText(WebElement webElement) {
		try {
			return driver.findElement((By) webElement).getText();
		} catch (NoSuchElementException e) {
			return null;
		}

	}

	/**
	 * Radio Button
	 **/
	public void radioButton(String radioButton, By locator) {
		// driver.findElement(locator).click();

		List<WebElement> radio = driver.findElements(locator);

		for (int i = 0; i < radio.size(); i++) {
			WebElement local_radio = radio.get(i);

			String valueRadio = local_radio.getAttribute("value");

			// System.out.println("Valores Radios Button Edad: "+ valueEdad);
			// mayor14 //mayor18
			if (valueRadio.equalsIgnoreCase(radioButton)) {
				local_radio.click();
			}
		}

	}
	
	
	/**
	 * Radio Button
	 **/
	public void listElementText(String textElement, By locator) {
		// driver.findElement(locator).click();

		List<WebElement> element = driver.findElements(locator);

		for (int i = 0; i < element.size(); i++) {
			WebElement elem = element.get(i);

			String valueElement = elem.getText();

		  System.out.println("Valores Radios Button Edad: "+ valueElement);
			// mayor14 //mayor18
			if (valueElement.equalsIgnoreCase(textElement)) {
				elem.click();
			}
		}

	}

	/**
	 * select Option Box
	 **/
	public void selectOption(String option, By locator) {

		// Select deliveryHourBox = new Select(driver.findElement(By.name("quicksearch_car_pickup_time")));
		// deliveryHourBox.selectByValue(deliveryHour.get(2));

		// Select licenseBox = new Select(driver.findElement(By.name("licenseFrom")));
		// licenseBox.selectByValue(license.get(2));
		Select optionSel = new Select(driver.findElement(locator));
		optionSel.selectByValue(option);
			}
	
	
	public String selectOptionText(String typeText, By locator) {
		Select indicative = new Select(driver.findElement(locator));
		indicative.selectByVisibleText(typeText);
	//	indicative.selectByVisibleText("+ 93");
		return getText(indicative.getFirstSelectedOption());
		
		//return getText(selectList.getFirsSelectedOption());
		
	}
	
	
    /**
	 * sleep
	 **/
	public void sleep(int sleep) throws InterruptedException {
		Thread.sleep(sleep);
	}

	/*
	 * Excel - GetCellData
	 */

	
	/*
	 * Excel - Write Excel
	 */
	

	/*
	 * Json get value File
	 */


}
