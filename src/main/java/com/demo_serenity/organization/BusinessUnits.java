package com.demo_serenity.organization;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.demo_serenity.base.SeleniumWrapper;

public class BusinessUnits extends SeleniumWrapper {

	public BusinessUnits(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	By buttonNewBusinessUnit = By.xpath("//*[@id='GridDiv']/div[2]/div[2]/div/div/div[1]/div/span/i");
	By titleModalNewBusinessUnit = By.id("ui-id-1");
	By buttonSave = By.xpath("//*[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Toolbar']//*[@class='fa fa-check-circle text-purple']");
	By inputNameBusinessUnit = By.id("Serenity_Pro_Organization_BusinessUnitDialog3_Name");
	By inputParentUnit = By.id("select2-chosen-1");
	By listParentUnit = By.id("select2-results-1");
	By inputCompleteParentUnit = By.id("s2id_autogen1_search");
	By elementsListParentUnit = By.xpath("//*[@class='select2-results-dept-0 select2-result select2-result-selectable']/div/font/font"); //text
    By inputSearchBusinessUnit = By.xpath("//*[@id='GridDiv']/div[2]/div[1]/input");
	
	public void clickbuttonNewBusinessUnit()
	{
		waitForElementPresent(buttonNewBusinessUnit);
		click(buttonNewBusinessUnit);
		waitForElementPresent(titleModalNewBusinessUnit);
	}
	
	public void setBusinessUnit(String nameBusinessUnit, String nameParentUnit, String listElement)
	{
		waitForElementPresent(inputNameBusinessUnit);
		type(nameBusinessUnit, inputNameBusinessUnit);
		click(inputParentUnit);
		waitForElementPresent(listParentUnit);
		type(nameParentUnit, inputCompleteParentUnit);
		listElementText(listElement, listParentUnit);
	}
	
	public void saveBusinessUnit()
	{
		waitForElementPresent(buttonSave);
		click(buttonSave);
	}
}
